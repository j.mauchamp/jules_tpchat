#include <iostream>
#include "Ctriangle.h"
using namespace std;

int main()
{
	Ctriangle tete(10, 100, 110, 100, 60, 300);
	Ctriangle nez(50, 10, 70, 30, 30, 30);

	tete.Afficher();
	tete.Deplacer(10, 20);
	tete.Afficher();

	nez.Deplacer(10, 20);
	nez.Afficher();
}
