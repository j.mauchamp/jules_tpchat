#pragma once
class Ctriangle
{
public:
	Ctriangle(short px1 = 0,
		short py1 = 0,
		short px2 = 0,
		short py2 = 0,
		short px3 = 0,
		short py3 = 0);

		void Afficher(void);
		void Deplacer(short, short);
		void Dessiner(void);

	~Ctriangle();

private:
	short x1, y1;
	short x2, y2;
	short x3, y3;
};

