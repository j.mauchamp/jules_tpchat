#include <iostream>
#include "Ctriangle.h"
using namespace std;

Ctriangle::Ctriangle(
	short px1, short py1,
	short px2, short py2,
	short px3, short py3) :
	x1(px1), y1(py1),
	x2(px2), y2(py2),
	x3(px3), y3(py3) 
	{
		cout << "Passage par le constructeur de triangle" << endl;
	};

	void Ctriangle::Afficher(void)
	{
		cout << "Coordonnes S1 = " << x1 << " et " << y1 << endl;
		cout << "Coordonnes S2 = " << x2 << " et " << y2 << endl;
		cout << "Coordonnes S3 = " << x3 << " et " << y3 << "\n" << endl;
	}

	void Ctriangle::Deplacer(short px, short py)
	{
		x1 = x1 + px;
		y1 = y1 + py;
		x2 = x2 + px;
		y2 = y2 + py;
		x3 = x3 + px;
		y3 = y3 + py;
	}

	Ctriangle::~Ctriangle()
	{
		cout << "Passage par le constructeur de triangle" << endl;
	}